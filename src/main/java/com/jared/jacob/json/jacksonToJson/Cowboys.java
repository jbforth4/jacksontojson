package com.jared.jacob.json.jacksonToJson;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

import java.util.List;

@JsonTypeName("Cowboys")
@Data
public class Cowboys {
    private List<OffensivePlayers> offensivePlayers;
    private List<DefensivePlayers> defensivePlayers;

}
