package com.jared.jacob.json.jacksonToJson;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@RestController
public class JsonController {

    private JsonMapper jsonMapper;
    private Teams teams;

    @Autowired
    public JsonController(JsonMapper jsonMapper){
        this.jsonMapper = jsonMapper;
    }

    @PostMapping(value = "json", consumes = "application/json")
    public ResponseEntity processJson(@RequestBody String json){
        log.info("Processing Event");
        if (StringUtils.isNotBlank(json)) {
            teams = jsonMapper.parseJson(json);
            log.info("json: " + json);
            jsonMapper.convertToJson(teams);
            log.info("teams: " + teams);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } else {
            log.info("Error processing json data");
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
