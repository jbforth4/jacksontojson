package com.jared.jacob.json.jacksonToJson;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

@Data
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = OffensivePlayers.class, name = "OffensivePlayers"),
        @JsonSubTypes.Type(value = DefensivePlayers.class, name = "DefensivePlayers")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Teams {

    @JsonBackReference
    @JsonProperty("type")
    private String type;
}
