package com.jared.jacob.json.jacksonToJson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class JsonMapper {

    public Teams parseJson(String json) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping();
        mapper.findAndRegisterModules();
        Teams teams = null;
        try {
            teams = mapper.readValue(json, Teams.class);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return teams;
    }

    public String convertToJson(Teams teams){
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.setDateFormat(new ISO8601DateFormat());
        String eventJson = "";
        try {
            eventJson = mapper.writeValueAsString(teams);
            log.info("eventJson: " + eventJson);

        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return eventJson;
    }
}
