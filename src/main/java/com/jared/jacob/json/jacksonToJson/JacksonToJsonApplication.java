package com.jared.jacob.json.jacksonToJson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonToJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(JacksonToJsonApplication.class, args);
	}
}
